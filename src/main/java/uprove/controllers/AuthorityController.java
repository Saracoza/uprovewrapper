package uprove.controllers;

import com.microsoft.uprove.*;
import uprove.exceptions.UProveException;

public class AuthorityController
{
    private final byte[] deviceMessage = null;

    private IssuerKeyAndParameters issuerKeyAndParameters;
    private IssuerParameters       issuerParameters;
    private Issuer                 issuer;



    public AuthorityController()
    {
        issuerKeyAndParameters = uprove.storage.AuthoritySettings.getInstance().getIssuerKeyAndParameters();
        issuerParameters       = uprove.storage.AuthoritySettings.getInstance().getIssuerParameters();
    }



    public void initialize(byte[] tokenInformation, byte[][] tokenAttributes) throws UProveException
    {
        try {
            IssuerProtocolParameters issuerProtocolParameters = new IssuerProtocolParameters();
            issuerProtocolParameters.setIssuerKeyAndParameters(issuerKeyAndParameters);
            issuerProtocolParameters.setTokenInformation(tokenInformation);
            issuerProtocolParameters.setTokenAttributes(tokenAttributes);

            issuer = issuerProtocolParameters.generate();
        } catch (Exception e) {
            throw new UProveException("Something happened!");
        }
    }



    public byte[][] generateFirstMessage() throws UProveException
    {
        try                 { return issuer.generateFirstMessage();             }
        catch (Exception e) { throw new UProveException("Something happened!"); }
    }

    public byte[][] generateThirdMessage(byte[][] secondMessage) throws UProveException
    {
        try                 { return issuer.generateThirdMessage(secondMessage); }
        catch (Exception e) { throw new UProveException("Something happened!");  }
        finally             { issuer = null;                                     }
    }



    @SuppressWarnings("ConstantConditions")
    public void verifyProof(byte[] message, UProveToken token, PresentationProof proof) throws UProveException
    {
        try {
            PresentationProtocol
                    .verifyPresentationProof(
                            issuerParameters,
                            new int[] {8,10},
                            message,
                            deviceMessage,
                            token,
                            proof
                    );
        } catch (Exception e) {
            throw new UProveException("Something happened!");
        }
    }
}
