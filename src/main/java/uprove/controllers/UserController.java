package uprove.controllers;

import com.microsoft.uprove.*;
import uprove.exceptions.UProveException;

public class UserController
{
    private final byte[] deviceMessage = null;

    private IssuerParameters issuerParameters;
    private Prover           prover;



    public UserController(IssuerParameters issuerParameters) throws UProveException
    {
        this.issuerParameters = issuerParameters;
        try                 { this.issuerParameters.validate();                 }
        catch (Exception e) { throw new UProveException("Something happened!"); }
    }



    public void initialize(byte[] tokenInformation, byte[][] tokenAttributes) throws UProveException
    {
        try {
            ProverProtocolParameters proverProtocolParameters = new ProverProtocolParameters();
            proverProtocolParameters.setIssuerParameters(issuerParameters);
            proverProtocolParameters.setTokenInformation(tokenInformation);
            proverProtocolParameters.setTokenAttributes(tokenAttributes);

            // known only
            // by  prover
            proverProtocolParameters.setProverInformation("eVoteClient V1.0 BETA by Andrei Stan".getBytes());

            prover = proverProtocolParameters.generate();
        } catch (Exception e) {
            throw new UProveException("Something happened!");
        }
    }



    public byte[][] generateSecondMessage(byte[][] firstMessage) throws UProveException
    {
        try                 { return prover.generateSecondMessage(firstMessage); }
        catch (Exception e) { throw new UProveException("Something happened!");  }
    }

    public UProveKeyAndToken generateToken(byte[][] thirdMessage) throws UProveException
    {
        try                 { return prover.generateTokens(thirdMessage)[0];    }
        catch (Exception e) { throw new UProveException("Something happened!"); }
        finally             { prover = null;                                    }
    }



    @SuppressWarnings("ConstantConditions")
    public PresentationProof generateProof(byte[] message, UProveKeyAndToken uProveKeyAndToken, byte[][] tokenAttributes) throws UProveException
    {
        try {
            return PresentationProtocol
                    .generatePresentationProof(
                            issuerParameters,
                            new int[] {8,10},
                            message,
                            deviceMessage,
                            uProveKeyAndToken,
                            tokenAttributes
                    );
        } catch (Exception e) {
            throw new UProveException("Something happened!");
        }
    }



    public void clearPrivKeyToken(UProveKeyAndToken keyAndToken)
    {
        if (keyAndToken == null) return;

        java.util.Arrays.fill(keyAndToken.getTokenPrivateKey(),                (byte) 0);
        java.util.Arrays.fill(keyAndToken.getToken().getIssuerParametersUID(), (byte) 0);
        java.util.Arrays.fill(keyAndToken.getToken().getPublicKey(),           (byte) 0);
        java.util.Arrays.fill(keyAndToken.getToken().getTokenInformation(),    (byte) 0);
        java.util.Arrays.fill(keyAndToken.getToken().getProverInformation(),   (byte) 0);
        java.util.Arrays.fill(keyAndToken.getToken().getSigmaZ(),              (byte) 0);
        java.util.Arrays.fill(keyAndToken.getToken().getSigmaC(),              (byte) 0);
        java.util.Arrays.fill(keyAndToken.getToken().getSigmaR(),              (byte) 0);
    }
}
