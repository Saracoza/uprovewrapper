package uprove.exceptions;

public class UProveException extends Exception
{
    public UProveException(String message)
    {
        super(message);
    }
}
