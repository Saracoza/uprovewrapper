package uprove.storage;

import com.microsoft.uprove.*;

public class AuthoritySettings
{
    private IssuerKeyAndParameters issuerKeyAndParameters;
    private IssuerParameters       issuerParameters;



    private AuthoritySettings() throws Exception
    {
        try {
            IssuerSetupParameters issuerSetupParameters = new IssuerSetupParameters();
            issuerSetupParameters.setParametersUID(java.util.UUID.randomUUID().toString().getBytes());
            issuerSetupParameters.setSpecification("eVoteServer V1.0 BETA by Andrei Stan".getBytes());
            issuerSetupParameters.setEncodingBytes(new byte[] {1,1,1,1,1,1,1,0,1,0});
            issuerSetupParameters.setHashAlgorithmUID("SHA-256");

            issuerKeyAndParameters = issuerSetupParameters.generate();
            issuerParameters       = issuerKeyAndParameters.getIssuerParameters();



            var issuerPrivateKeyFile = new java.io.File(util.FilesManager.getInstance().getFilePath("issuerPrivateKey.bin"));
            var issuerParametersFile = new java.io.File(util.FilesManager.getInstance().getFilePath("issuerParameters.bin"));

            if (issuerPrivateKeyFile.length() != 0 &&
                issuerParametersFile.length() != 0) {

                var privateKeyStream = new java.io.FileInputStream(issuerPrivateKeyFile);
                var parametersStream = new java.io.FileInputStream(issuerParametersFile);

                issuerKeyAndParameters.setPrivateKey(privateKeyStream.readAllBytes());
                issuerKeyAndParameters.setIssuerParameters(
                        uprove.converters.Deserializer.deserializeIssuerParameters(
                                new String(parametersStream.readAllBytes())
                        )
                );
                issuerParameters = issuerKeyAndParameters.getIssuerParameters();

                privateKeyStream.close();
                parametersStream.close();
            }
            else {
                var privateKeyStream = new java.io.FileOutputStream(issuerPrivateKeyFile);
                var parametersStream = new java.io.FileOutputStream(issuerParametersFile);

                privateKeyStream.write(issuerKeyAndParameters.getPrivateKey());
                parametersStream.write(
                        uprove.converters.Serializer.serializeIssuerParameters(
                                issuerParameters
                        ).getBytes()
                );

                privateKeyStream.close();
                parametersStream.close();
            }
        } catch (Exception e) {
            throw new Exception("An error occurred during issuer parameters setup.");
        }
    }



    private static AuthoritySettings instance;
    public  static void createInstance() throws Exception
    {
        if (instance == null) {
            instance = new AuthoritySettings();
        }
    }
    public  static AuthoritySettings getInstance()
    {
        return instance;
    }



    public IssuerKeyAndParameters getIssuerKeyAndParameters()
    {
        return issuerKeyAndParameters;
    }

    public IssuerParameters getIssuerParameters()
    {
        return issuerParameters;
    }
}
