package uprove.storage;

import com.microsoft.uprove.*;

import javax.crypto.*;
import javax.crypto.spec.*;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

public class TokensManager
{
    private final String formattedPasswordQuery
            = ansi().fg(CYAN).a("Password:").reset().toString();

    private final int iterationsCount = 1024;
    private final int keyLength       = 256;
    private final int ZERO            = 0;

    private Console console = System.console();

    private String storedTokensDirectory =
            System.getProperty("user.home")
                    + System.getProperty("file.separator")
                    + "Documents"
                    + System.getProperty("file.separator")
                    + "eVoteClient"
                    + System.getProperty("file.separator")
                    + "Stored Tokens";



    private TokensManager() throws IOException
    {
        // get the path of the directory in
        // which the tokens  will be stored
        var storedTokensDirPath = Paths.get(storedTokensDirectory);

        // if directories along the path
        // do  not  exist,  create  them
        if (!Files.exists(storedTokensDirPath)) Files.createDirectories(storedTokensDirPath);
    }



    private static TokensManager instance = null;
    public  static void createInstance() throws IOException
    {
        if (instance == null) {
            instance = new TokensManager();
        }
    }
    public  static TokensManager getInstance()
    {
        return instance;
    }



    public void listTokens()
    {
        var storedTokensFile = new File(storedTokensDirectory);
        var storedTokensList = storedTokensFile.listFiles();

        // if the stored tokens list
        // is null, set  length to 0
        var tokensListLength = storedTokensList != null ? storedTokensList.length : 0;

        for (int i = 0; i < tokensListLength; ++i) System.out.println(storedTokensList[i].getName());
    }



    public void storeToken(UProveKeyAndToken uProveKeyAndToken)
    {
        try {
            var privateKey = uProveKeyAndToken.getTokenPrivateKey();
            var token      = uProveKeyAndToken.getToken();


            var keyAndTokenFile = new File(storedTokensDirectory + System.getProperty("file.separator") + token.hashCode());
            if (keyAndTokenFile.createNewFile()) {
                OutputStream outputStream = new FileOutputStream(keyAndTokenFile);


                byte[] iv   = new byte[16];
                byte[] salt = new byte[16];
                java.security.SecureRandom.getInstance("SHA1PRNG").nextBytes(  iv);
                java.security.SecureRandom.getInstance("SHA1PRNG").nextBytes(salt);
                outputStream.write(  iv);
                outputStream.write(salt);


                ByteBuffer byteBuffer = ByteBuffer.allocate(4);
                byteBuffer.putInt(ZERO, privateKey.length);                     outputStream.write(byteBuffer.array());
                byteBuffer.putInt(ZERO, token.getIssuerParametersUID().length); outputStream.write(byteBuffer.array());
                byteBuffer.putInt(ZERO, token.getPublicKey().length);           outputStream.write(byteBuffer.array());
                byteBuffer.putInt(ZERO, token.getTokenInformation().length);    outputStream.write(byteBuffer.array());
                byteBuffer.putInt(ZERO, token.getProverInformation().length);   outputStream.write(byteBuffer.array());
                byteBuffer.putInt(ZERO, token.getSigmaZ().length);              outputStream.write(byteBuffer.array());
                byteBuffer.putInt(ZERO, token.getSigmaC().length);              outputStream.write(byteBuffer.array());
                byteBuffer.putInt(ZERO, token.getSigmaR().length);              outputStream.write(byteBuffer.array());


                IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);


                SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
                System.out.print(formattedPasswordQuery);
                char[] password = console.readPassword();
                PBEKeySpec pbeKeySpec = new PBEKeySpec(password, salt, iterationsCount, keyLength);
                // zero  the  password  to  minimize  the
                // lifetime of  sensitive data  in memory
                java.util.Arrays.fill(password, ' ');
                SecretKeySpec secretKeySpec = new SecretKeySpec(
                        keyFactory.generateSecret(pbeKeySpec).getEncoded(), "AES"
                );
                // zero  the  password  to  minimize  the
                // lifetime of  sensitive data  in memory
                pbeKeySpec.clearPassword();


                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
                outputStream.write(cipher.update(privateKey,                     ZERO, privateKey.length                    ));
                outputStream.write(cipher.update(token.getIssuerParametersUID(), ZERO, token.getIssuerParametersUID().length));
                outputStream.write(cipher.update(token.getPublicKey(),           ZERO, token.getPublicKey().length          ));
                outputStream.write(cipher.update(token.getTokenInformation(),    ZERO, token.getTokenInformation().length   ));
                outputStream.write(cipher.update(token.getProverInformation(),   ZERO, token.getProverInformation().length  ));
                outputStream.write(cipher.update(token.getSigmaZ(),              ZERO, token.getSigmaZ().length             ));
                outputStream.write(cipher.update(token.getSigmaC(),              ZERO, token.getSigmaC().length             ));
                outputStream.write(cipher.update(token.getSigmaR(),              ZERO, token.getSigmaR().length             ));
                outputStream.write(cipher.doFinal());


                outputStream.close();
            }
        } catch (Exception e) {
            System.out.print(ansi().fg(RED));
            System.out.println(
                    "Token storage failed! You will not"
                    + System.lineSeparator() +
                    "be able to reuse it to vote again."
            );
            System.out.print(ansi().reset());
        }
    }

    public UProveKeyAndToken loadToken(int tokenID) throws uprove.exceptions.UProveException
    {
        try {
            var keyAndTokenFile = new File(storedTokensDirectory + System.getProperty("file.separator") + tokenID);
            var inputStream     = new FileInputStream(keyAndTokenFile);


            byte[] iv   = inputStream.readNBytes(16);
            byte[] salt = inputStream.readNBytes(16);


            var lengths = new int[8];
            var data = new byte[8][];
            byte[] encKeyAndToken;
            byte[] decKeyAndToken;
            for (int i = 0; i < 8; ++i) {
                // read  lengths
                // from the file
                lengths[i] = ByteBuffer.wrap(inputStream.readNBytes(4)).getInt();

                // alloc for corresponding buffer
                // the  length   previously  read
                data[i] = new byte[lengths[i]];
            }
            encKeyAndToken = inputStream.readAllBytes();


            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);


            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            System.out.print(formattedPasswordQuery);
            char[] password = console.readPassword();
            PBEKeySpec pbeKeySpec = new PBEKeySpec(password, salt, iterationsCount, keyLength);
            // zero  the  password  to  minimize  the
            // lifetime of  sensitive data  in memory
            java.util.Arrays.fill(password, ' ');
            SecretKeySpec secretKeySpec = new SecretKeySpec(
                    keyFactory.generateSecret(pbeKeySpec).getEncoded(), "AES"
            );
            // zero  the  password  to  minimize  the
            // lifetime of  sensitive data  in memory
            pbeKeySpec.clearPassword();


            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
            decKeyAndToken = cipher.doFinal(encKeyAndToken);


            System.arraycopy(decKeyAndToken, ZERO, data[0], ZERO, lengths[0]);
            int totalLength = ZERO;
            for (int i = 1; i < 8; ++i) {
                totalLength = totalLength + lengths[i - 1];
                System.arraycopy(decKeyAndToken, totalLength, data[i], ZERO, lengths[i]);
            }


            inputStream.close();


            var keyAndToken = new UProveKeyAndToken(
                    new UProveToken(
                            data[1],
                            data[2],
                            data[3],
                            data[4],
                            data[5],
                            data[6],
                            data[7],
                            false
                    ),
                    data[0]
            );


            // zero the key and token buffers to minimize
            // the lifetime  of sensitive  data in memory
            java.util.Arrays.fill(decKeyAndToken, (byte) 0);


            return keyAndToken;

        } catch (Exception e) {
            throw new uprove.exceptions.UProveException(
                    "Token loading failed! Make sure"
                    + System.lineSeparator() +
                    "that the  token ID  is correct!"
            );
        }
    }
}
