package uprove.converters;

import com.microsoft.uprove.*;
import org.javatuples.Triplet;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.zip.DataFormatException;

public class Deserializer
{
    public static byte[][] base64ToBytes(String data) throws DataFormatException
    {
        try {
            var base64s = data.split(" ");
            var base64Decoder = java.util.Base64.getDecoder();

            byte[][] bytes = new byte[base64s.length][];
            int bytesIndex = 0;
            for (var base64 : base64s) bytes[bytesIndex++] = base64Decoder.decode(base64);

            return bytes;

        } catch (Exception e) {
            throw new DataFormatException("Invalid data!");
        }
    }



    public static IssuerParameters deserializeIssuerParameters(String serializedIssuerParameters) throws DataFormatException
    {
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(serializedIssuerParameters);

            var base64Decoder = java.util.Base64.getDecoder();

            var ip = new IssuerParameters();
            ip.setParametersUID(base64Decoder.decode((String) jsonObject.get("parametersUID")));
            ip.setSpecification(base64Decoder.decode((String) jsonObject.get("specification")));
            ip.setEncodingBytes(base64Decoder.decode((String) jsonObject.get("encodingBytes")));
            ip.setHashAlgorithmUID((String) jsonObject.get("hashAlgorithmUID"));
            ip.setGroup(new Subgroup(
                    base64Decoder.decode((String) jsonObject.get("groupP")),
                    base64Decoder.decode((String) jsonObject.get("groupQ")),
                    base64Decoder.decode((String) jsonObject.get("groupG"))
            ));
            ip.setPublicKey(base64ToBytes((String) jsonObject.get("publicKey")));
            ip.setProverIssuanceValues(base64ToBytes((String) jsonObject.get("proverIssuanceValues")));

            return ip;

        } catch (Exception e) {
            throw new DataFormatException("Invalid data!");
        }
    }

    public static UProveToken deserializeToken(String serializedToken) throws DataFormatException
    {
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(serializedToken);

            var base64Decoder = java.util.Base64.getDecoder();

            return new UProveToken(
                    base64Decoder.decode((String) jsonObject.get("issuerParametersUID")),
                    base64Decoder.decode((String) jsonObject.get("publicKey")),
                    base64Decoder.decode((String) jsonObject.get("tokenInformation" )),
                    base64Decoder.decode((String) jsonObject.get("proverInformation")),
                    base64Decoder.decode((String) jsonObject.get("sigmaZ")),
                    base64Decoder.decode((String) jsonObject.get("sigmaC")),
                    base64Decoder.decode((String) jsonObject.get("sigmaR")),
                    false
            );
        } catch (Exception e) {
            throw new DataFormatException("Invalid data!");
        }
    }

    public static PresentationProof deserializeProof(String serializedProof) throws DataFormatException
    {
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(serializedProof);

            var base64Decoder = java.util.Base64.getDecoder();

            // rd is  different from  null only
            // if the proof is device protected
            var rd = jsonObject.containsKey("rd") ? base64Decoder.decode((String) jsonObject.get("rd")) : null;

            return new PresentationProof(
                    base64ToBytes((String) jsonObject.get("disclosedAttributes")),
                    base64Decoder.decode((String) jsonObject.get("a" )),
                    base64Decoder.decode((String) jsonObject.get("r0")),
                    base64ToBytes((String) jsonObject.get("r")),
                    rd // different from null only if proof is device protected
            );
        } catch (Exception e) {
            throw new DataFormatException("Invalid data!");
        }
    }

    public static Triplet<byte[], UProveToken, PresentationProof> deserializeMessageTokenProof(String serializedMessageTokenProof) throws DataFormatException
    {
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(serializedMessageTokenProof);

            var base64Decoder = java.util.Base64.getDecoder();

            var message = base64Decoder.decode((String) jsonObject.get("message"));
            var token   = deserializeToken((String) jsonObject.get("serializedToken"));
            var proof   = deserializeProof((String) jsonObject.get("serializedProof"));

            return new Triplet<>(message, token, proof);

        } catch (Exception e) {
            throw new DataFormatException("Invalid data!");
        }
    }
}
