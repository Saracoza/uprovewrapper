package uprove.converters;

import com.microsoft.uprove.*;
import org.json.simple.JSONObject;

public class Serializer
{
    public static String bytesToBase64(byte[][] data)
    {
        StringBuilder stringBuilder = new StringBuilder();
        var base64Encoder = java.util.Base64.getEncoder();

        for (var bytes : data) {
            stringBuilder.append(base64Encoder.encodeToString(bytes));
            stringBuilder.append(" ");
        }
        stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(" "));

        return stringBuilder.toString();
    }



    @SuppressWarnings("unchecked")
    public static String serializeIssuerParameters(IssuerParameters issuerParameters)
    {
        JSONObject serializedIP = new JSONObject();

        var base64Encoder = java.util.Base64.getEncoder();

        serializedIP.put("parametersUID", base64Encoder.encodeToString(issuerParameters.getParametersUID()));
        serializedIP.put("encodingBytes", base64Encoder.encodeToString(issuerParameters.getEncodingBytes()));
        serializedIP.put("specification", base64Encoder.encodeToString(issuerParameters.getSpecification()));
        serializedIP.put("hashAlgorithmUID", issuerParameters.getHashAlgorithmUID());
        serializedIP.put("groupP", base64Encoder.encodeToString(((Subgroup) issuerParameters.getGroup()).getP().toByteArray()));
        serializedIP.put("groupQ", base64Encoder.encodeToString(((Subgroup) issuerParameters.getGroup()).getQ().toByteArray()));
        serializedIP.put("groupG", base64Encoder.encodeToString(((Subgroup) issuerParameters.getGroup()).getG().toByteArray()));
        serializedIP.put("publicKey", bytesToBase64(issuerParameters.getPublicKey()));
        serializedIP.put("proverIssuanceValues", bytesToBase64(issuerParameters.getProverIssuanceValues()));

        return serializedIP.toJSONString();
    }

    @SuppressWarnings("unchecked")
    public static String serializeToken(UProveToken uProveToken)
    {
        JSONObject serializedToken = new JSONObject();

        var base64Encoder = java.util.Base64.getEncoder();

        serializedToken.put("issuerParametersUID", base64Encoder.encodeToString(uProveToken.getIssuerParametersUID()));
        serializedToken.put("publicKey", base64Encoder.encodeToString(uProveToken.getPublicKey()));
        serializedToken.put("tokenInformation",  base64Encoder.encodeToString(uProveToken.getTokenInformation() ));
        serializedToken.put("proverInformation", base64Encoder.encodeToString(uProveToken.getProverInformation()));
        serializedToken.put("sigmaZ", base64Encoder.encodeToString(uProveToken.getSigmaZ()));
        serializedToken.put("sigmaC", base64Encoder.encodeToString(uProveToken.getSigmaC()));
        serializedToken.put("sigmaR", base64Encoder.encodeToString(uProveToken.getSigmaR()));

        return serializedToken.toJSONString();
    }

    @SuppressWarnings("unchecked")
    public static String serializeProof(PresentationProof presentationProof)
    {
        JSONObject serializedProof = new JSONObject();

        var base64Encoder = java.util.Base64.getEncoder();

        serializedProof.put("disclosedAttributes", bytesToBase64(presentationProof.getDisclosedAttributes()));
        serializedProof.put("a" , base64Encoder.encodeToString(presentationProof.getA() ));
        serializedProof.put("r0", base64Encoder.encodeToString(presentationProof.getR0()));
        serializedProof.put("r" , bytesToBase64(presentationProof.getR()));
        //serializedProof.put("rd", base64Encoder.encodeToString(presentationProof.getRd()));

        return serializedProof.toJSONString();
    }

    @SuppressWarnings("unchecked")
    public static String serializeMessageTokenProof(byte[] message, UProveToken token, PresentationProof proof)
    {
        JSONObject serializedMessageTokenProof = new JSONObject();

        var base64Encoder = java.util.Base64.getEncoder();

        serializedMessageTokenProof.put("message", base64Encoder.encodeToString(message));
        serializedMessageTokenProof.put("serializedToken", serializeToken(token));
        serializedMessageTokenProof.put("serializedProof", serializeProof(proof));

        return serializedMessageTokenProof.toJSONString();
    }
}
